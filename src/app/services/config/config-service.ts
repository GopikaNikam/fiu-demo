import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private appConfig: any;

  constructor(private http: HttpClient) { }

  loadAppConfig() {
    return this.http.get('assets/config.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }

  get apiBaseUrl() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.apiBaseUrl;
  }

  get onboardingUrl() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.onboardingUrl;
  }
  
  get username() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.username;
  }

  get password() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.password;
  }

  get fiuId() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.fiuId;
  }

  get userPostfixName() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.userPostfixName;
  }

  get channelId() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.channelId;
  }

  get redirectUrl() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.redirectUrl;
  }
  
  get consentDescription() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.consentDescription;
  }

  get templateName() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.templateName;
  }

  get aaId() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.aaId;
  }
}
