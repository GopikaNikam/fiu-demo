import { TestBed } from '@angular/core/testing';

import { WebviewEncryptRequestService } from './webview-encrypt-request.service';

describe('WebviewEncryptRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebviewEncryptRequestService = TestBed.get(WebviewEncryptRequestService);
    expect(service).toBeTruthy();
  });
});
