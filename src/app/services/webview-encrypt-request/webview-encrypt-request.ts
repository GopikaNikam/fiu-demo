export interface WebviewEncryptRequest {
  txnId: String
  sessionId: String
  srcRef: String
  userId: String
  redirectUrl: String
  fiuId: String
}
  
export interface WebViewEncryptedRequest {
	encryptedRequest: string
  requestDate: string
  encryptedFiuId: string
  aaId: string
}

	