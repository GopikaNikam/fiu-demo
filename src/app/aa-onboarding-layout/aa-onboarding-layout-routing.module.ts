import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplyLoanComponent } from './apply-loan/apply-loan.component';
import { LoanEligibilityComponent } from './loan-eligibility/loan-eligibility.component';
import { LoanSuccessComponent } from './loan-success/loan-success.component';
import { LoanFailureComponent } from './loan-failure/loan-failure.component';
import { ConsentRejectComponent } from './consent-reject/consent-reject.component';
import { IframeComponent } from './iframe-component/iframe-component';

const routes: Routes = [
  { 
    path: 'onboarding-layout', 
    component: ApplyLoanComponent 
  },
  { 
    path: 'loan-eligibility', 
    component: LoanEligibilityComponent 
  },
  { 
    path: 'loan-success', 
    component: LoanSuccessComponent 
  },
  { 
    path: 'loan-failure', 
    component: LoanFailureComponent 
  },
  { 
    path: 'consent-reject', 
    component: ConsentRejectComponent 
  },
  { 
    path: 'iframe-component', 
    component: IframeComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AaOnboardingLayoutRoutingModule { }
